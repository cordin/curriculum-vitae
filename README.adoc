= Plantilla Asciidoctor para tu CV

Ejemplo de CV hecho en Asciidoctor

- toda la potencia de Asciidoctor (parrafos, iconos, tablas, capitulos,etc)
- 13 temas a aplicar (rocket-panda por defecto)
- HTML y Pdf
- integrado con Gitlab CI para la generación
- integrado con Gitlab pages para su publicacion on-line (si quieres)


== Requisitos

Si vas a usar Gitlab sólo necesitas crear un proyecto con tu cuenta.

Si el nombre del proyecto es *tu-usuario.gitlab.io* puedes hacer que tu CV
sea tu página principal sin tener que incluir el nombre del proyecto en la ruta

== Resultado

En http://jorge-aguilera.gitlab.io/asciidoctor-cv-template/index.html puedes ver
un ejemplo, y en http://jorge-aguilera.gitlab.io/asciidoctor-cv-template/template.pdf
como se vería en PDF